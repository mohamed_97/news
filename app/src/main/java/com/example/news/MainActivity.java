package com.example.news;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;

import com.example.news.Models.NewsApiResponse;
import com.example.news.Models.NewsHeadlines;
import com.example.news.Utiels.OnFetchDataListener;
import com.example.news.Utiels.ReguestManger;
import com.example.news.Utiels.SelectListener;
import com.example.news.VeiwModel.CustomAdapter;
import com.example.news.View.DetailsMainActivity;

import java.util.List;

public class MainActivity extends AppCompatActivity implements SelectListener {

    RecyclerView recyclerView;
    CustomAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
      ReguestManger reguestManger=new ReguestManger(this);
      reguestManger.getNewsHeadLines(listener,"general",null);
    }
    private final OnFetchDataListener<NewsApiResponse> listener=new OnFetchDataListener<NewsApiResponse>() {
        @Override
        public Void onfetchdata(List<NewsHeadlines> list, String message) {
         shownews(list);
         return null;
        }

        @Override
        public void onError(String message) {

        }
    };

    private void shownews(List<NewsHeadlines> list) {

        recyclerView=findViewById(R.id.recycler_main);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(this,1));
        adapter=new CustomAdapter(this,list,this);
        recyclerView.setAdapter(adapter);



    }

    @Override
    public void onNewsclicked(NewsHeadlines newsHeadlines) {

        startActivity(new Intent(this, DetailsMainActivity.class).putExtra("data",
                newsHeadlines));


    }
}