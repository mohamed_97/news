package com.example.news.Utiels;

import com.example.news.Models.NewsHeadlines;

public interface SelectListener {
    void onNewsclicked(NewsHeadlines newsHeadlines);
}
