package com.example.news;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;

import com.example.news.Models.NewsHeadlines;
import com.example.news.View.DetailsMainActivity;
import com.squareup.picasso.Picasso;

public class ImgMainActivity3 extends AppCompatActivity {
    String url;
    ImageView  imageView;
    NewsHeadlines newsHeadlines;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_img_main3);
        // dataw mean data will be deliver to image actvity
        // data ean data will be deliver to detail  actvity
        newsHeadlines=(NewsHeadlines) getIntent().getSerializableExtra("dataw");
        url=newsHeadlines.getUrlToImage();

        imageView=findViewById(R.id.imgw);

        Picasso.get().load(url).into(imageView);


        imageView.setOnClickListener(view -> {
            startActivity(new Intent(this, DetailsMainActivity.class).putExtra("data",
                    newsHeadlines));

        });


    }
}