package com.example.news.View;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.news.ImgMainActivity3;
import com.example.news.Models.NewsHeadlines;
import com.example.news.R;
import com.squareup.picasso.Picasso;

public class DetailsMainActivity extends AppCompatActivity {
         NewsHeadlines newsHeadlines;
         TextView txt_detail,txt_authar,txt_time,txt_content,txt_titel;
         ImageView img_news;
         Button button;
         String url;
         int cont=1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_main);
        newsHeadlines=(NewsHeadlines) getIntent().getSerializableExtra("data");
        txt_authar=findViewById(R.id.text_detail_authar);
        txt_content=findViewById(R.id.text_detail_content);
        txt_detail=findViewById(R.id.text_detail_detail);
        txt_time=findViewById(R.id.text_detail_time);
        txt_titel=findViewById(R.id.text_detail_titel);
        button=findViewById(R.id.readmore);
        img_news=findViewById(R.id.img__detail_news);

        txt_authar.setText(newsHeadlines.getAuthor());
        txt_titel.setText(newsHeadlines.getTitle());
        txt_time.setText(newsHeadlines.getPublishedAt());
        txt_content.setText(newsHeadlines.getContent());
        txt_detail.setText(newsHeadlines.getDescription());
        Picasso.get().load(newsHeadlines.getUrlToImage()).into(img_news);
        url=newsHeadlines.getUrl();
       String urlimg=newsHeadlines.getUrlToImage();
        button.setOnClickListener(view -> {

            // go to url
            Intent imgIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(imgIntent);


        });


        img_news.setOnClickListener(view -> {

            startActivity(new Intent(this, ImgMainActivity3.class).putExtra("dataw",
                    newsHeadlines));

        });

    }
}